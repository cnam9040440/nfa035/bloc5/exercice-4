# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 

## Objectifs
* Mises en application :
- [x] (Exercice 1) Service REST : Lecture d'un Badge
- [x] (Exercice 2) Service REST : suppression d'un Badge
- [x] (Exercice 3) Service REST : Ajout d'un Badge
- [x] **(Exercice 4) "Défragmentation" : Nettoyage des lignes supprimées par reconstitution d'une base propre**

----

Afin d'implémenter le service de suppression de badge, abordons le problème par impacts, couche par couche.

## Impacts

### Impact sur le Modèle

### Impact sur la désérialisation des Métadonnées

### Impact sur la sérialisation
      
### Impact sur Les DAOs
     
### Impact sur la couche de Service

## Tests

### Unitaires

### D'acceptance (de recette)

1. Ajout d'un badge avec valeurs par défaut
![](screenshots/put1.png)
2. Retour du service
![](screenshots/put2.png)
- On observe bien un code de retour 201, avec le lien vers l'instance créée
3. Consultation de l'instance créée
![](screenshots/put3.png)
- On observe bien l'objet demandé
4. Retour du service avec modification de la description
![](screenshots/put4.png)
- On observe bien le code retour 200
5. COnsultation de l'instance modifiée
![](screenshots/put5.png)
   - On observe bien la modification

  
**FIN**

----

## Ressources supplémentaires


## Défragmentation  (REMIS à PLUS TARD => non noté pour le moment, mais sujet d'examen blanc potentiel)

- [ ] Développer un traitement "batch" permettant de nettoyer le fichier de base de données Json suite à un certain nombre de suppressions
    - [ ] Il s'agit de lire le fichier de base json octet par octet en sautant les lignes supprimées (comme nous en connaissons les positions), et d'écrire cela dans la foulée vers un nouveau fichier, temporaire
    - [ ] En post-traitement il conviendra d'archiver la base originale, puis de renommer le fichier temporaire du nom du fichier original








